//SERVER

import com.sun.net.httpserver.HttpServer;

import java.util.*;
import java.net.*;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;

public class Main {

    //convert into JSON
    public static String customerToJSON(ArrayList customer){
        ObjectMapper mapper = new ObjectMapper();
        String s = "";

        try {
            s = mapper.writeValueAsString(customer);
        } catch (JsonProcessingException error) {
            System.err.println(error.toString());
        }
        return s;
    }


    public static void main(String[] args) {
        //starting variables
        int i=0;
        //create customers to compile into JSON
        Customer cust1 = new Customer("Jimmy", 12456);
        Customer cust2 = new Customer("Sally", 57665);
        ArrayList<Customer> customers = new ArrayList<>();
        customers.add(cust1);
        customers.add(cust2);
        String json = Main.customerToJSON(customers);
        System.out.println(json);

        //create a server that listen to requests
        try {
            System.out.println("Starting up.");
            final ServerSocket serverSocket = new ServerSocket(0001);
            while (i==0) {
                System.out.println("Listening...");
                final Socket socket = serverSocket.accept();
                //socket.getInputStream(); //we don't care about receiving data from the client
                final OutputStream outputStream = socket.getOutputStream(); //create output object
                final PrintWriter printWriter = new PrintWriter(outputStream); //send string object
                System.out.println("Connected.");
                printWriter.println(json); //send the data we have
                printWriter.flush(); //wait for client to receive
                System.out.println("Sent.");
                socket.close(); //end the connection
                if (i==0) {i=1;} //exits the loop because we have no conditions
            }
        } catch (IOException e) {
            System.out.println("Failure. Shame.");
        }


    }

}



/*
//create a connection object
    private static HttpURLConnection connect;

    //Method 1: java.net.HttpURLConnection

    //create objects to store data
    BufferedReader read;
    String line;
    StringBuffer respond = new StringBuffer();

        try { //attempt to establish a connection to the target site

                //placeholder website
                URL url = new URL("https://localhost:0001");

                //activate connection object
                connect = (HttpURLConnection) url.openConnection();

                //set object paremeters
                connect.setRequestMethod("GET");
                connect.setConnectTimeout(5000);
                connect.setReadTimeout(5000);

                //get the status of the connection
                int status=connect.getResponseCode();
                //System.out.println(status);

                if (status > 299) { //if the status tells us the connect is not working
                read = new BufferedReader(new InputStreamReader(connect.getErrorStream()));
                while ((line = read.readLine()) != null) {
                //while there are things to read, display them
                respond.append(line);
                }
                //close the reader after the loop is finished
                read.close();
                } else { //if the status tells us it is working
                read = new BufferedReader(new InputStreamReader(connect.getInputStream()));
                while ((line = read.readLine()) != null) {
                //while there are things to read, display them
                respond.append(line);
                }
                //close the reader
                read.close();
                }
                //all that stuff appended to the respond string? print it all out
                System.out.println(respond.toString());
                //time to parse JSON
                parse(respond.toString());

                //the try/catch is required for these functions to work, as failure is inevitable
                } catch (MalformedURLException malformed) {
                System.err.println(malformed.toString());
                } catch (IOException connect) {
                System.err.println(connect.toString());
                } finally {
                //no matter what, at the end, disconnect
                connect.disconnect();
                }
        */
/*
public class Main {

    public static void main(String[] args) {
	    HttpServer server = getServer();
	    System.out.println("Server running...");
	    server.start();
    }

    public static HttpServer getServer() {
        return null;
    }

}

public class HTTPExample {
    public static String getHttpContent(String string) {

        String content = "";

        try {
            URL url = new URL(string);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();

            BufferedReader reader = new BufferedReader();
            StringBuilder stringBuilder = new StringBuilder();

            String line = null;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line + "\n");
            }
            content = stringBuilder.toString();

        } catch (Exception e) {
            System.err.println(e.toString());
        }
        return content;
    }

    public static Map getHttpHeaders(String string) {
        Map hashmap = null;

        try {
            URL url = new URL(string);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();

            hashmap = http.getHeaderFields();

        } catch (Exception g) {
            System.err.println(g.toString());
        }
        return hashmap;

    }
}
*/

/*
    public static void main(String[] args) {
        System.out.println(HTTPExample.getHttpContent("http://localhost:0001"));
        Map<Integer, String>m=HTTPExample.getHttpHeaders("http://localhost:0001");
        for (Map.Entry<Integer, String> entry: m.entrySet()) {
            System.out.println("Key = "+entry.getKey()+"Value = "+entry.getValue());
        }
    }

}*/
